import { createI18n } from 'vue-i18n'
import messages from '@intlify/unplugin-vue-i18n/messages'
import type { SettingsRepository } from '@/settings/domain/repositories/SettingsRepository'
import type { Lang } from '@/settings/domain/value-objects/Locale'
import { SettingsRepositoryFactory } from '@/settings/infrastructure/data/factories/SettingsRepositoryFactory'

const settingsStore: SettingsRepository = SettingsRepositoryFactory.create()
const locale = settingsStore.get().locale.code

export const i18n = createI18n({
  locale,
  messages,
  legacy: false
})

export function updateLocale(code: Lang): Lang {
  i18n.global.locale.value = code

  return code
}
