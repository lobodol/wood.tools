import { createRouter, createWebHashHistory } from 'vue-router'
import HygroscopicBalanceView from '@/views/hygroscopy/HygroscopicBalanceView.vue'
import MinMaxHygroscopicBalanceView from '@/views/hygroscopy/MinMaxHygroscopicBalanceView.vue'
import SettingsView from '@/views/SettingsView.vue'
import ShrinkageView from '@/views/shrinkage/ShrinkageView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HygroscopicBalanceView
    },
    {
      path: '/hygroscopic-balance/min-max',
      name: 'minMaxHygroscopicBalance',
      component: MinMaxHygroscopicBalanceView
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsView
    },
    {
      path: '/shrinkage',
      name: 'shrinkage',
      component: ShrinkageView
    }
  ]
})

export default router
