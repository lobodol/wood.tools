export enum Unit {
  Percent,
  Ratio
}

export default class Moisture {
  readonly value: number
  readonly unit: Unit

  constructor(value: number, unit: Unit) {
    if (value < 0) throw new Error('Invalid value, cannot be negative')

    this.value = value
    this.unit = unit
  }

  toPercentage(): Moisture {
    if (this.unit === Unit.Percent) return this

    return new Moisture(this.value * 100, Unit.Percent)
  }

  toRatio(): Moisture {
    if (this.unit === Unit.Ratio) return this

    return new Moisture(this.value / 100, Unit.Ratio)
  }
}
