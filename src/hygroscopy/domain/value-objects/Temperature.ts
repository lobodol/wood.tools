export enum Unit {
  Fahrenheit,
  Celcius
}

export default class Temperature {
  readonly unit: Unit
  readonly value: number

  constructor(value: number, unit: Unit) {
    this.unit = unit
    this.value = value
  }

  toFahrenheit(): Temperature {
    if (this.unit == Unit.Fahrenheit) return this

    return new Temperature((9 * this.value) / 5 + 32, Unit.Fahrenheit)
  }

  toCelcius(): Temperature {
    if (this.unit == Unit.Celcius) return this

    return new Temperature((5 * (this.value - 32)) / 9, Unit.Celcius)
  }

  toUnit(unit: Unit): Temperature {
    return unit === Unit.Fahrenheit ? this.toFahrenheit() : this.toCelcius()
  }
}
