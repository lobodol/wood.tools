import Temperature from '@/hygroscopy/domain/value-objects/Temperature'

export default class TemperatureRange {
  public readonly min: Temperature
  public readonly max: Temperature

  constructor(min: Temperature, max: Temperature) {
    this.min = min
    this.max = max
  }
}
