import Moisture from '@/hygroscopy/domain/value-objects/Moisture'

export default class MoistureRange {
  public readonly min: Moisture
  public readonly max: Moisture

  constructor(min: Moisture, max: Moisture) {
    this.min = min
    this.max = max
  }
}
