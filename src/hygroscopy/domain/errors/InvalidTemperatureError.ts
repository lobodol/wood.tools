import type Temperature from '@/hygroscopy/domain/value-objects/Temperature'

export class InvalidTemperatureError extends Error {
  constructor(readonly temperature: Temperature) {
    super()
  }
}
