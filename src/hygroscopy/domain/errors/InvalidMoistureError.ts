import type Moisture from '@/hygroscopy/domain/value-objects/Moisture'

export class InvalidMoistureError extends Error {
  constructor(readonly moisture: Moisture) {
    super()
  }
}
