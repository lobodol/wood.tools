import TemperatureRange from '@/hygroscopy/domain/value-objects/TemperatureRange'
import MoistureRange from '@/hygroscopy/domain/value-objects/MoistureRange'
import Temperature from '@/hygroscopy/domain/value-objects/Temperature'
import Moisture, { Unit } from '@/hygroscopy/domain/value-objects/Moisture'
import { InvalidTemperatureError } from '@/hygroscopy/domain/errors/InvalidTemperatureError'
import { InvalidMoistureError } from '@/hygroscopy/domain/errors/InvalidMoistureError'

/**
 * Compute hygroscopic balance for given temperature and relative moisture
 * Implementation of Hailwood-Horrobin equation
 *
 * https://fr.wikipedia.org/wiki/%C3%89quilibre_hygrom%C3%A9trique
 */
export default function hygroscopicBalance(temperature: Temperature, moisture: Moisture): Moisture {
  const fahrenheit = temperature.toFahrenheit().value
  const humidity = moisture.toRatio().value

  if (fahrenheit < 0 || fahrenheit > 200) {
    throw new InvalidTemperatureError(temperature)
  }

  if (humidity > 0.95) {
    throw new InvalidMoistureError(moisture)
  }

  const W = 330 + 0.452 * fahrenheit + 0.00415 * Math.pow(fahrenheit, 2)
  const k =
    0.791 + 4.63 * Math.pow(10, -4) * fahrenheit - 8.44 * Math.pow(10, -7) * Math.pow(fahrenheit, 2)
  const k1 =
    6.34 + 7.75 * Math.pow(10, -4) * fahrenheit - 9.35 * Math.pow(10, -5) * Math.pow(fahrenheit, 2)
  const k2 =
    1.09 + 2.84 * Math.pow(10, -2) * fahrenheit - 9.04 * Math.pow(10, -5) * Math.pow(fahrenheit, 2)

  const coefficient = 1800 / W
  const partA = (k * humidity) / (1 - k * humidity)
  const partB =
    (k1 * k * humidity + 2 * k1 * k2 * Math.pow(k, 2) * Math.pow(humidity, 2)) /
    (1 + k1 * k * humidity + k1 * k2 * Math.pow(k, 2) * Math.pow(humidity, 2))

  return new Moisture(coefficient * (partA + partB), Unit.Percent)
}

export function minMaxHygroscopicBalance(
  temperature: TemperatureRange,
  moisture: MoistureRange
): MoistureRange {
  const min = hygroscopicBalance(temperature.max, moisture.min)
  const max = hygroscopicBalance(temperature.min, moisture.max)

  return new MoistureRange(min, max)
}
