import { TemperatureFactory } from '@/hygroscopy/presentation/factories/TemperatureFactory'
import { SettingsRepositoryFactory } from '@/settings/infrastructure/data/factories/SettingsRepositoryFactory'
import { computed, ref } from 'vue'
import type { Ref, ComputedRef } from 'vue'
import type { Temperature as VTemp } from '@/hygroscopy/presentation/models/Temperature'
import hygroscopicBalance from '@/hygroscopy/domain/use-cases/HygroscopicBalance'
import { TemperatureMapper } from '@/hygroscopy/presentation/mappers/TemperatureMapper'
import Moisture, { Unit as MUnit } from '@/hygroscopy/domain/value-objects/Moisture'
import Temperature, { Unit } from '@/hygroscopy/domain/value-objects/Temperature'

type ViewProps = {
  temperature: Ref<VTemp>
  moisture: Ref<number>
  result: ComputedRef<string>
  getMaxAllowedTemperature: () => number
}

export const useHygroscopicBalanceView = (): ViewProps => {
  const temperatureFactory = new TemperatureFactory(SettingsRepositoryFactory.create())

  const temperature = ref<VTemp>(temperatureFactory.createDefault())
  const moisture = ref(45)

  const getMaxAllowedTemperature = (): number => {
    const temp = TemperatureMapper.viewToDomain(temperature.value)
    const max = new Temperature(90, Unit.Celcius).toUnit(temp.unit)

    return max.value
  }

  const result = computed((): string => {
    return hygroscopicBalance(
      TemperatureMapper.viewToDomain(temperature.value),
      new Moisture(moisture.value, MUnit.Percent)
    )
      .toPercentage()
      .value.toPrecision(2)
  })

  return {
    temperature,
    moisture,
    result,
    getMaxAllowedTemperature
  }
}
