import { computed, ref } from 'vue'
import type { Ref, ComputedRef } from 'vue'
import { minMaxHygroscopicBalance } from '@/hygroscopy/domain/use-cases/HygroscopicBalance'
import TemperatureRange from '@/hygroscopy/domain/value-objects/TemperatureRange'
import type { Temperature as VTemperature } from '@/hygroscopy/presentation/models/Temperature'
import MoistureRange from '@/hygroscopy/domain/value-objects/MoistureRange'
import Moisture, { Unit as MUnit } from '@/hygroscopy/domain/value-objects/Moisture'
import { TemperatureFactory } from '@/hygroscopy/presentation/factories/TemperatureFactory'
import { SettingsRepositoryFactory } from '@/settings/infrastructure/data/factories/SettingsRepositoryFactory'
import { TemperatureMapper } from '@/hygroscopy/presentation/mappers/TemperatureMapper'
import Temperature, { Unit } from '@/hygroscopy/domain/value-objects/Temperature'

type ViewProps = {
  minTemp: Ref<VTemperature>
  maxTemp: Ref<VTemperature>
  minMoisture: Ref<number>
  maxMoisture: Ref<number>
  moisture: ComputedRef<MoistureRange>
  maxAllowedTemperature: number
}

export const useMinMaxHygroscopicBalanceView = (): ViewProps => {
  const temperatureFactory = new TemperatureFactory(SettingsRepositoryFactory.create())

  const minTemp = ref<VTemperature>(temperatureFactory.createDefault())
  const maxTemp = ref<VTemperature>({ ...minTemp.value, value: minTemp.value.value + 10 })
  const minMoisture = ref(30)
  const maxMoisture = ref(75)

  const maxAllowedTemperature = (): number => {
    const temp = TemperatureMapper.viewToDomain(minTemp.value)
    const max = new Temperature(90, Unit.Celcius).toUnit(temp.unit)

    return max.value
  }

  const moisture = computed(() => {
    return minMaxHygroscopicBalance(
      new TemperatureRange(
        TemperatureMapper.viewToDomain(minTemp.value),
        TemperatureMapper.viewToDomain(maxTemp.value)
      ),
      new MoistureRange(
        new Moisture(minMoisture.value, MUnit.Percent),
        new Moisture(maxMoisture.value, MUnit.Percent)
      )
    )
  })

  return {
    minTemp,
    maxTemp,
    minMoisture,
    maxMoisture,
    moisture,
    maxAllowedTemperature: maxAllowedTemperature()
  }
}
