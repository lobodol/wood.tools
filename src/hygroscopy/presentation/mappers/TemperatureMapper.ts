import Temperature from '@/hygroscopy/domain/value-objects/Temperature'
import type { Temperature as VTemperature } from '@/hygroscopy/presentation/models/Temperature'
import { Unit } from '@/hygroscopy/domain/value-objects/Temperature'
import { Unit as VUnit } from '@/hygroscopy/presentation/models/Temperature'

export class TemperatureMapper {
  static viewToDomain(temperature: VTemperature): Temperature {
    return new Temperature(
      temperature.value,
      temperature.unit === VUnit.Celcius ? Unit.Celcius : Unit.Fahrenheit
    )
  }

  static domainToView(temperature: Temperature): VTemperature {
    return {
      value: temperature.value,
      unit: temperature.unit === Unit.Celcius ? VUnit.Celcius : VUnit.Fahrenheit
    }
  }
}
