export enum Unit {
  Celcius = '°C',
  Fahrenheit = '°F'
}

export type Temperature = {
  value: number
  unit: Unit
}
