import type { Temperature as VTemp } from '@/hygroscopy/presentation/models/Temperature'
import { Unit } from '@/hygroscopy/presentation/models/Temperature'
import type { SettingsRepository } from '@/settings/domain/repositories/SettingsRepository'
import { TemperatureMapper } from '@/hygroscopy/presentation/mappers/TemperatureMapper'

export class TemperatureFactory {
  constructor(private readonly settings: SettingsRepository) {}

  createDefault(): VTemp {
    const unit = this.resolveUnit()
    const value = unit === Unit.Celcius ? 20 : 68

    return {
      value,
      unit
    }
  }

  /**
   * Create a view model Temperature using the given one for default value and resolving the unit from the global settings.
   *
   * @param {number} value Default value in °C
   *
   * @example Given default value is 30°C and the unit system is imperial, the result will be a 86°F view model
   */
  createWithDefault(value: number): VTemp {
    const result = TemperatureMapper.viewToDomain({ value: value, unit: Unit.Celcius })
    const defaultUnit = TemperatureMapper.viewToDomain(this.createDefault())

    return TemperatureMapper.domainToView(result.toUnit(defaultUnit.unit))
  }

  private resolveUnit(): Unit {
    return this.settings.get().locale.code === 'fr' ? Unit.Celcius : Unit.Fahrenheit
  }
}
