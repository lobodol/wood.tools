import { TemperatureMapper } from '@/hygroscopy/presentation/mappers/TemperatureMapper'
import Temperature, { Unit as DUnit } from '@/hygroscopy/domain/value-objects/Temperature'
import { Unit as VUnit } from '@/hygroscopy/presentation/models/Temperature'

test('View to domain', () => {
  const actual = TemperatureMapper.viewToDomain({ value: 23, unit: VUnit.Celcius })
  const expected = new Temperature(23, DUnit.Celcius)

  expect(actual).toStrictEqual(expected)
})

test('Domain to view', () => {
  const actual = TemperatureMapper.domainToView(new Temperature(40, DUnit.Fahrenheit))
  const expected = { value: 40, unit: VUnit.Fahrenheit }

  expect(actual).toStrictEqual(expected)
})
