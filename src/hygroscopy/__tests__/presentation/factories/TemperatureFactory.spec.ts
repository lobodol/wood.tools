import { TemperatureFactory } from '@/hygroscopy/presentation/factories/TemperatureFactory'
import { Unit } from '@/hygroscopy/presentation/models/Temperature'

describe('TemperatureFactory', (): void => {
  const settingsMock = { get: jest.fn(), save: jest.fn() }
  const factory = new TemperatureFactory(settingsMock)

  describe('createDefault', () => {
    test('For French language expecting °C', (): void => {
      settingsMock.get.mockReturnValue({ locale: { code: 'fr' } })
      const actual = factory.createDefault()
      const expected = { value: 20, unit: Unit.Celcius }

      expect(actual).toStrictEqual(expected)
    })

    test('For English language expecting °F', (): void => {
      settingsMock.get.mockReturnValue({ locale: { code: 'en' } })
      const actual = factory.createDefault()
      const expected = { value: 68, unit: Unit.Fahrenheit }

      expect(actual).toStrictEqual(expected)
    })
  })

  describe('createWithDefault', () => {
    test('For French language expecting °C', () => {
      settingsMock.get.mockReturnValue({ locale: { code: 'fr' } })

      const actual = factory.createWithDefault(24)
      const expected = { value: 24, unit: Unit.Celcius }

      expect(actual).toStrictEqual(expected)
    })

    test('For English language expecting °F', () => {
      settingsMock.get.mockReturnValue({ locale: { code: 'en' } })

      const actual = factory.createWithDefault(23)
      const expected = { value: 73.4, unit: Unit.Fahrenheit }

      expect(actual).toStrictEqual(expected)
    })
  })
})
