import Moisture, { Unit } from '../../../domain/value-objects/Moisture'

describe('Invalid argument', () => {
  test('Negative value', () => {
    expect(() => new Moisture(-1, Unit.Percent)).toThrow('Invalid value, cannot be negative')
  })
})

describe('Conversion', () => {
  test('Ratio to percent', () => {
    const moisture = new Moisture(0.5, Unit.Ratio)
    expect(moisture.toPercentage().value).toEqual(50)
  })

  test('Percent to ratio', () => {
    const moisture = new Moisture(50, Unit.Percent)
    expect(moisture.toRatio().value).toEqual(0.5)
  })
})

describe('Self-conversion', () => {
  test('Ratio to ratio', () => {
    const moisture = new Moisture(0.5, Unit.Ratio)
    expect(moisture.toRatio()).toBe(moisture)
  })

  test('Percent to percent', () => {
    const moisture = new Moisture(50, Unit.Percent)
    expect(moisture.toPercentage()).toBe(moisture)
  })
})
