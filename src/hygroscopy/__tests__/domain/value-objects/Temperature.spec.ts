import Temperature, { Unit } from '../../../domain/value-objects/Temperature'

describe('Conversion', (): void => {
  test('°C to °F', () => {
    const temperature = new Temperature(30, Unit.Celcius)

    expect(temperature.toFahrenheit().value).toEqual(86)
  })

  test('°F to °C', (): void => {
    const temperature = new Temperature(86, Unit.Fahrenheit)

    expect(temperature.toCelcius().value).toEqual(30)
  })

  test('°F to given unit', (): void => {
    const temperature = new Temperature(86, Unit.Fahrenheit)

    expect(temperature.toUnit(Unit.Celcius).value).toEqual(30)
  })

  test('°C to given unit', (): void => {
    const temperature = new Temperature(30, Unit.Celcius)

    expect(temperature.toUnit(Unit.Fahrenheit).value).toEqual(86)
  })
})

describe('Self-conversion', (): void => {
  test('°C to °C', () => {
    const temperature = new Temperature(30, Unit.Celcius)

    expect(temperature.toCelcius()).toBe(temperature)
  })

  test('°F to °F', () => {
    const temperature = new Temperature(30, Unit.Fahrenheit)

    expect(temperature.toFahrenheit()).toBe(temperature)
  })
})
