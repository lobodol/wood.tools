import Moisture, { Unit as MUnit } from '../../../domain/value-objects/Moisture'
import Temperature, { Unit } from '../../../domain/value-objects/Temperature'
import MoistureRange from '../../../domain/value-objects/MoistureRange'
import TemperatureRange from '../../../domain/value-objects/TemperatureRange'
import hygroscopicBalance, {
  minMaxHygroscopicBalance
} from '../../../domain/use-cases/HygroscopicBalance'
import { InvalidTemperatureError } from '@/hygroscopy/domain/errors/InvalidTemperatureError'
import { InvalidMoistureError } from '@/hygroscopy/domain/errors/InvalidMoistureError'

describe('Invalid parameters', (): void => {
  test('Temperature too high', (): void => {
    expect(() => {
      hygroscopicBalance(new Temperature(201, Unit.Fahrenheit), new Moisture(0.5, MUnit.Ratio))
    }).toThrow(InvalidTemperatureError)
  })

  test('Temperature too low', (): void => {
    expect(() => {
      hygroscopicBalance(new Temperature(-1, Unit.Fahrenheit), new Moisture(0.5, MUnit.Ratio))
    }).toThrow(InvalidTemperatureError)
  })

  test('Moisture too high', (): void => {
    expect(() => {
      hygroscopicBalance(new Temperature(35, Unit.Celcius), new Moisture(96, MUnit.Percent))
    }).toThrow(InvalidMoistureError)
  })
})

describe('Compute', () => {
  test('hygroscopicBalance', (): void => {
    const result = hygroscopicBalance(
      new Temperature(35, Unit.Celcius),
      new Moisture(0.47, MUnit.Ratio)
    )

    expect(result.value).toEqual(8.339844530749739)
  })

  test('minMaxHygroscopicBalance', (): void => {
    const result = minMaxHygroscopicBalance(
      new TemperatureRange(new Temperature(16, Unit.Celcius), new Temperature(28, Unit.Celcius)),
      new MoistureRange(new Moisture(0.3, MUnit.Ratio), new Moisture(0.75, MUnit.Ratio))
    )

    expect(result.min.value).toEqual(6.03411637035941)
    expect(result.max.value).toEqual(14.605483737788637)
  })
})
