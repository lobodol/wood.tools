import { Wood } from '@/shrinkage/domain/entities/Wood'
import type { InMemoryWood } from '@/shrinkage/infrastructure/data/dto/InMemoryWood'

export class WoodMapper {
  dataToDomain(object: InMemoryWood): Wood {
    return new Wood(
      object.name,
      object.radialShrinkageCoefficient,
      object.tangentialShrinkageCoefficient
    )
  }
}
