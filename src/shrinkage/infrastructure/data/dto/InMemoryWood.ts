export type InMemoryWood = {
  name: string
  radialShrinkageCoefficient: number
  tangentialShrinkageCoefficient: number
}
