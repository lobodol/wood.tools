import type { WoodRepository } from '@/shrinkage/domain/repositories/WoodRepository'
import type { Wood } from '@/shrinkage/domain/entities/Wood'
import type { WoodMapper } from '@/shrinkage/infrastructure/data/WoodMapper'
import type { InMemoryWood } from '@/shrinkage/infrastructure/data/dto/InMemoryWood'

const woods: Array<InMemoryWood> = [
  {
    name: 'oak',
    radialShrinkageCoefficient: 0.17,
    tangentialShrinkageCoefficient: 0.33
  },
  {
    name: 'beech',
    radialShrinkageCoefficient: 0.2,
    tangentialShrinkageCoefficient: 0.4
  },
  {
    name: 'poplar',
    radialShrinkageCoefficient: 0.16,
    tangentialShrinkageCoefficient: 0.3
  },
  {
    name: 'ash',
    radialShrinkageCoefficient: 0.178,
    tangentialShrinkageCoefficient: 0.3
  },
  {
    name: 'maple',
    radialShrinkageCoefficient: 0.13,
    tangentialShrinkageCoefficient: 0.26
  },
  {
    name: 'walnut',
    radialShrinkageCoefficient: 0.09,
    tangentialShrinkageCoefficient: 0.14
  },
  {
    name: 'chestnut',
    radialShrinkageCoefficient: 0.13,
    tangentialShrinkageCoefficient: 0.26
  },
  {
    name: 'linden',
    radialShrinkageCoefficient: 0.18,
    tangentialShrinkageCoefficient: 0.3
  },
  {
    name: 'alder',
    radialShrinkageCoefficient: 0.15,
    tangentialShrinkageCoefficient: 0.3
  },
  {
    name: 'elm',
    radialShrinkageCoefficient: 0.28,
    tangentialShrinkageCoefficient: 0.19
  },
  {
    name: 'robinia',
    radialShrinkageCoefficient: 0.2,
    tangentialShrinkageCoefficient: 0.38
  },
  {
    name: 'cherry',
    radialShrinkageCoefficient: 0.2,
    tangentialShrinkageCoefficient: 0.33
  },
  {
    name: 'fir_tree',
    radialShrinkageCoefficient: 0.14,
    tangentialShrinkageCoefficient: 0.27
  },
  {
    name: 'spruce',
    radialShrinkageCoefficient: 0.14,
    tangentialShrinkageCoefficient: 0.28
  },
  {
    name: 'red_cedar',
    radialShrinkageCoefficient: 0.09,
    tangentialShrinkageCoefficient: 0.22
  },
  {
    name: 'maritime_pine',
    radialShrinkageCoefficient: 0.16,
    tangentialShrinkageCoefficient: 0.27
  },
  {
    name: 'scots_pine',
    radialShrinkageCoefficient: 0.13,
    tangentialShrinkageCoefficient: 0.26
  },
  {
    name: 'douglas',
    radialShrinkageCoefficient: 0.17,
    tangentialShrinkageCoefficient: 0.25
  },
  {
    name: 'larch',
    radialShrinkageCoefficient: 0.16,
    tangentialShrinkageCoefficient: 0.3
  },
  {
    name: 'carolina_pine',
    radialShrinkageCoefficient: 0.16,
    tangentialShrinkageCoefficient: 0.26
  },
  {
    name: 'afzelia',
    radialShrinkageCoefficient: 0.1,
    tangentialShrinkageCoefficient: 0.16
  },
  {
    name: 'padouk',
    radialShrinkageCoefficient: 0.1,
    tangentialShrinkageCoefficient: 0.2
  },
  {
    name: 'ipe',
    radialShrinkageCoefficient: 0.255,
    tangentialShrinkageCoefficient: 0.32
  },
  {
    name: 'meranti',
    radialShrinkageCoefficient: 0.153,
    tangentialShrinkageCoefficient: 0.27
  },
  {
    name: 'sipo',
    radialShrinkageCoefficient: 0.153,
    tangentialShrinkageCoefficient: 0.27
  },
  {
    name: 'merbau',
    radialShrinkageCoefficient: 0.153,
    tangentialShrinkageCoefficient: 0.27
  },
  {
    name: 'azobe',
    radialShrinkageCoefficient: 0.26,
    tangentialShrinkageCoefficient: 0.36
  },
  {
    name: 'okoume',
    radialShrinkageCoefficient: 0.13,
    tangentialShrinkageCoefficient: 0.16
  },
  {
    name: 'rubberwood',
    radialShrinkageCoefficient: 0.09,
    tangentialShrinkageCoefficient: 0.23
  }
]

export class InMemoryWoodRepository implements WoodRepository {
  readonly mapper: WoodMapper

  constructor(mapper: WoodMapper) {
    this.mapper = mapper
  }

  findWoods(): Promise<Wood[]> {
    return new Promise<Wood[]>((resolve) => {
      resolve(woods.map((item) => this.mapper.dataToDomain(item)))
    })
  }
}
