import type { Length } from '@/shrinkage/domain/value-objects/Length'

export class NegativeLengthError extends Error {
  public readonly length

  constructor(length: Length) {
    super()
    this.length = length
  }
}
