import { WoodBoard } from '../value-objects/WoodBoard'
import { ShrinkageCalculator } from '../services/ShrinkageCalculator'
import { Length } from '../value-objects/Length'
import MoistureRange from '@/hygroscopy/domain/value-objects/MoistureRange'

type MinMaxValues = {
  min: Length
  max: Length
}

export type MinMaxLengths = {
  radialLength: MinMaxValues
  tangentialLength: MinMaxValues
}

export const ComputeMinMaxShrinkage = (
  board: WoodBoard,
  targetMoisture: MoistureRange
): MinMaxLengths => {
  const calculator = new ShrinkageCalculator()

  const minVariations = calculator.calculate(board, targetMoisture.min)
  const maxVariations = calculator.calculate(board, targetMoisture.max)

  return {
    radialLength: {
      min: new Length(
        board.radialLength.value - minVariations.radialVariation.value,
        board.radialLength.unit
      ),
      max: new Length(
        board.radialLength.value - maxVariations.radialVariation.value,
        board.radialLength.unit
      )
    },
    tangentialLength: {
      min: new Length(
        board.tangentialLength.value - minVariations.tangentialVariation.value,
        board.tangentialLength.unit
      ),
      max: new Length(
        board.tangentialLength.value - maxVariations.tangentialVariation.value,
        board.tangentialLength.unit
      )
    }
  }
}
