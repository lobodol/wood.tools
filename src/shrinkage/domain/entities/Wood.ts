export class Wood {
  readonly name: string
  readonly radialShrinkageCoefficient: number
  readonly tangentialShrinkageCoefficient: number

  constructor(
    name: string,
    radialShrinkageCoefficient: number,
    tangentialShrinkageCoefficient: number
  ) {
    this.name = name
    this.radialShrinkageCoefficient = radialShrinkageCoefficient
    this.tangentialShrinkageCoefficient = tangentialShrinkageCoefficient
  }
}
