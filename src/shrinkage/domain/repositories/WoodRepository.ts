import { Wood } from '@/shrinkage/domain/entities/Wood'

export interface WoodRepository {
  findWoods(): Promise<Wood[]>
}
