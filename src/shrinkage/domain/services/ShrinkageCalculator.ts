import { Length } from '@/shrinkage/domain/value-objects/Length'
import type { WoodBoard } from '@/shrinkage/domain/value-objects/WoodBoard'
import Moisture, { Unit } from '@/hygroscopy/domain/value-objects/Moisture'

export type ShrinkageVariation = {
  radialVariation: Length
  tangentialVariation: Length
}

export class ShrinkageCalculator {
  calculate(woodBoard: WoodBoard, targetMoisture: Moisture): ShrinkageVariation {
    const radialVariation = this.radialShrinkage(
      woodBoard.radialLength,
      woodBoard.moisture,
      targetMoisture,
      woodBoard.radialShrinkageCoefficient
    )

    const tangentialVariation = this.computeShrinkage(
      woodBoard.tangentialLength,
      woodBoard.moisture,
      targetMoisture,
      woodBoard.tangentialShrinkageCoefficient
    )

    return {
      radialVariation,
      tangentialVariation
    }
  }

  private radialShrinkage(
    radialLength: Length,
    currentMoisture: Moisture,
    targetMoisture: Moisture,
    shrinkageCoefficient: number
  ): Length {
    const fiberSaturationPoint = 0.3

    // Over 30% moisture (known as fiber saturation point), there is no more radial shrinkage
    const moisture = new Moisture(
      Math.min(currentMoisture.toRatio().value, fiberSaturationPoint),
      Unit.Ratio
    )

    return this.computeShrinkage(radialLength, moisture, targetMoisture, shrinkageCoefficient)
  }

  /**
   * Compute shrinkage of the given length
   * A positive result means the length will shrink, a negative value means the length will expand
   */
  private computeShrinkage(
    length: Length,
    currentMoisture: Moisture,
    targetMoisture: Moisture,
    shrinkageCoefficient: number
  ): Length {
    const variation =
      length.value *
      (currentMoisture.toRatio().value - targetMoisture.toRatio().value) *
      shrinkageCoefficient

    return new Length(variation, length.unit)
  }
}
