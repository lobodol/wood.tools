export enum MetricUnit {
  Millimeter = 'mm',
  Centimeter = 'cm',
  Meter = 'm'
}

export enum ImperialUnit {
  Inch = 'in',
  Feet = 'ft',
  Yard = 'yd'
}

export type Unit = MetricUnit | ImperialUnit

export class Length {
  readonly unit: Unit
  readonly value: number

  constructor(value: number, unit: Unit) {
    this.value = value
    this.unit = unit
  }

  equals(length: Length): boolean {
    return this.unit === length.unit && this.value === length.value
  }

  add(length: Length): Length {
    if (length.unit !== this.unit) throw new Error(`Incompatible units`)

    return new Length(length.value + this.value, this.unit)
  }
}
