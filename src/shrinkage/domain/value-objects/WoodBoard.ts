import type { Length } from '@/shrinkage/domain/value-objects/Length'
import type { Wood } from '@/shrinkage/domain/entities/Wood'
import { NegativeLengthError } from '@/shrinkage/domain/errors/NegativeLengthError'
import Moisture from '@/hygroscopy/domain/value-objects/Moisture'

export class WoodBoard {
  private wood: Wood
  readonly radialLength: Length
  readonly tangentialLength: Length
  readonly moisture: Moisture

  constructor(wood: Wood, moisture: Moisture, radialLength: Length, tangentialLength: Length) {
    if (radialLength.value < 0) throw new NegativeLengthError(radialLength)
    if (tangentialLength.value < 0) throw new NegativeLengthError(tangentialLength)

    this.wood = wood
    this.moisture = moisture
    this.radialLength = radialLength
    this.tangentialLength = tangentialLength
  }

  get radialShrinkageCoefficient(): number {
    return this.wood.radialShrinkageCoefficient
  }

  get tangentialShrinkageCoefficient(): number {
    return this.wood.tangentialShrinkageCoefficient
  }
}
