import { ShrinkageCalculator } from '@/shrinkage/domain/services/ShrinkageCalculator'
import { Length, MetricUnit } from '@/shrinkage/domain/value-objects/Length'
import { Wood } from '@/shrinkage/domain/entities/Wood'
import { WoodBoard } from '@/shrinkage/domain/value-objects/WoodBoard'
import Moisture, { Unit } from '@/hygroscopy/domain/value-objects/Moisture'

describe('ShrinkCalculator', (): void => {
  const calculator = new ShrinkageCalculator()
  const board = new WoodBoard(
    new Wood('oak', 0.145, 0.31),
    new Moisture(9, Unit.Percent),
    new Length(20, MetricUnit.Centimeter),
    new Length(18, MetricUnit.Millimeter)
  )

  test('Calculate shrinkage of a board, expecting negative shrinkage (expansion)', (): void => {
    const actual = calculator.calculate(board, new Moisture(12, Unit.Percent))

    expect(actual.radialVariation.value).toBeCloseTo(-0.087)
    expect(actual.radialVariation.unit).toBe(MetricUnit.Centimeter)
    expect(actual.tangentialVariation.value).toBeCloseTo(-0.1674)
    expect(actual.tangentialVariation.unit).toBe(MetricUnit.Millimeter)
  })

  test('Calculate shrinkage of a board, expecting positive shrinkage', (): void => {
    const actual = calculator.calculate(board, new Moisture(7, Unit.Percent))

    expect(actual.radialVariation.value).toBeCloseTo(0.058)
    expect(actual.radialVariation.unit).toBe(MetricUnit.Centimeter)
    expect(actual.tangentialVariation.value).toBeCloseTo(0.1116)
    expect(actual.tangentialVariation.unit).toBe(MetricUnit.Millimeter)
  })
})
