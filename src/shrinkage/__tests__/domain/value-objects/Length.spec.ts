import { ImperialUnit, Length, MetricUnit } from '@/shrinkage/domain/value-objects/Length'

describe('Lengths with different units', (): void => {
  test('Add Centimeters to Inches, expecting error', (): void => {
    const length1 = new Length(10, MetricUnit.Centimeter)
    const length2 = new Length(10, ImperialUnit.Inch)

    expect(() => length1.add(length2)).toThrow('Incompatible units')
  })

  test('Assert 10cm do not equal 10in', (): void => {
    const length1 = new Length(10, MetricUnit.Centimeter)
    const length2 = new Length(10, ImperialUnit.Inch)

    expect(length1.equals(length2)).toEqual(false)
  })

  test('Assert 10cm do not equal 100mm', (): void => {
    const length1 = new Length(10, MetricUnit.Centimeter)
    const length2 = new Length(100, MetricUnit.Millimeter)

    expect(length1.equals(length2)).toEqual(false)
  })
})

describe('Lengths with same units', (): void => {
  test('Add 10cm to 2.4cm', (): void => {
    const length1 = new Length(10, MetricUnit.Centimeter)
    const length2 = new Length(2.4, MetricUnit.Centimeter)

    const expected = new Length(12.4, MetricUnit.Centimeter)

    expect(length1.add(length2)).toEqual(expected)
  })

  test('Assert 10cm equal 10cm', (): void => {
    const length1 = new Length(10, MetricUnit.Centimeter)
    const length2 = new Length(10, MetricUnit.Centimeter)

    expect(length1.equals(length2)).toEqual(true)
  })
})
