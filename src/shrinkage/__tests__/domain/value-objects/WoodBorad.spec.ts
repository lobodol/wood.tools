import { Length, MetricUnit } from '@/shrinkage/domain/value-objects/Length'
import { WoodBoard } from '@/shrinkage/domain/value-objects/WoodBoard'
import { Wood } from '@/shrinkage/domain/entities/Wood'
import { NegativeLengthError } from '@/shrinkage/domain/errors/NegativeLengthError'
import Moisture, { Unit } from '@/hygroscopy/domain/value-objects/Moisture'

describe('WoodBoard', (): void => {
  const wood = new Wood('oak', 0.1, 0.2)
  const moisture = new Moisture(30, Unit.Percent)

  it('Should raise an error with negative radial length', (): void => {
    const radialLength = new Length(-200, MetricUnit.Millimeter)
    const tangentialLength = new Length(90, MetricUnit.Millimeter)

    expect(() => new WoodBoard(wood, moisture, radialLength, tangentialLength)).toThrowError(
      NegativeLengthError
    )
  })

  it('Should raise an error with negative radial length', (): void => {
    const radialLength = new Length(200, MetricUnit.Millimeter)
    const tangentialLength = new Length(-90, MetricUnit.Millimeter)

    expect(() => new WoodBoard(wood, moisture, radialLength, tangentialLength)).toThrowError(
      NegativeLengthError
    )
  })
})
