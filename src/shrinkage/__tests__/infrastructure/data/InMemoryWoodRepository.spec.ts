import { WoodMapper } from '@/shrinkage/infrastructure/data/WoodMapper'
import { InMemoryWoodRepository } from '@/shrinkage/infrastructure/data/InMemoryWoodRepository'
import { Wood } from '@/shrinkage/domain/entities/Wood'

describe('In-memory wood repository', (): void => {
  test('findAll', async (): Promise<void> => {
    const mapper = new WoodMapper()
    const repository = new InMemoryWoodRepository(mapper)

    const woods = await repository.findWoods()
    expect(woods).toHaveLength(29)

    const expected = new Wood('oak', 0.17, 0.33)
    expect(woods[0]).toEqual(expected)
  })
})
