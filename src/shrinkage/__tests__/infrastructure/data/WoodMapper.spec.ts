import { WoodMapper } from '@/shrinkage/infrastructure/data/WoodMapper'
import { Wood } from '@/shrinkage/domain/entities/Wood'

describe('In-memory wood mapper', (): void => {
  test('Build domain object from data source', (): void => {
    const mapper = new WoodMapper()

    const actual = mapper.dataToDomain({
      name: 'Foo',
      radialShrinkageCoefficient: 2,
      tangentialShrinkageCoefficient: 3
    })

    const expected = new Wood('Foo', 2, 3)

    expect(actual).toEqual(expected)
  })
})
