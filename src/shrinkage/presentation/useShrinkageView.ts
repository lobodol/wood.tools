import { computed, ref } from 'vue'
import { InMemoryWoodRepository } from '@/shrinkage/infrastructure/data/InMemoryWoodRepository'
import type { Wood } from '@/shrinkage/domain/entities/Wood'
import { WoodMapper } from '@/shrinkage/infrastructure/data/WoodMapper'
import { Length, MetricUnit } from '@/shrinkage/domain/value-objects/Length'
import { WoodBoard } from '@/shrinkage/domain/value-objects/WoodBoard'
import { ComputeMinMaxShrinkage } from '@/shrinkage/domain/use-cases/ComputeMinMaxShrinkage'
import Moisture, { Unit as MUnit } from '@/hygroscopy/domain/value-objects/Moisture'
import MoistureRange from '@/hygroscopy/domain/value-objects/MoistureRange'
import TemperatureRange from '@/hygroscopy/domain/value-objects/TemperatureRange'
import { minMaxHygroscopicBalance } from '@/hygroscopy/domain/use-cases/HygroscopicBalance'
import { TemperatureFactory } from '@/hygroscopy/presentation/factories/TemperatureFactory'
import { SettingsRepositoryFactory } from '@/settings/infrastructure/data/factories/SettingsRepositoryFactory'
import { TemperatureMapper } from '@/hygroscopy/presentation/mappers/TemperatureMapper'

export const useShrinkageView = () => {
  const repository = new InMemoryWoodRepository(new WoodMapper())
  const temperatureFactory = new TemperatureFactory(SettingsRepositoryFactory.create())

  const woods = ref<Wood[]>([])

  // Board properties
  const selectedWood = ref<null | Wood>(null)
  const radialLength = ref<number>(200)
  const tangentialLength = ref<number>(18)
  const moisture = ref<number>(9)

  // Destination properties
  const minTemperature = ref(temperatureFactory.createWithDefault(17))
  const maxTemperature = ref(temperatureFactory.createWithDefault(20))
  const minHumidity = ref<number>(35)
  const maxHumidity = ref<number>(60)

  repository.findWoods().then((result) => {
    woods.value = result
    selectedWood.value = result[0]
  })

  const targetMoisture = computed(() =>
    minMaxHygroscopicBalance(
      new TemperatureRange(
        TemperatureMapper.viewToDomain(minTemperature.value),
        TemperatureMapper.viewToDomain(maxTemperature.value)
      ),
      new MoistureRange(
        new Moisture(minHumidity.value, MUnit.Percent),
        new Moisture(maxHumidity.value, MUnit.Percent)
      )
    )
  )

  const result = computed(() => {
    if (!selectedWood.value) return null

    const board = new WoodBoard(
      selectedWood.value,
      new Moisture(moisture.value, MUnit.Percent),
      new Length(radialLength.value, MetricUnit.Millimeter),
      new Length(tangentialLength.value, MetricUnit.Millimeter)
    )

    return ComputeMinMaxShrinkage(board, targetMoisture.value)
  })

  return {
    woods,
    selectedWood,
    radialLength,
    tangentialLength,
    moisture,
    minTemperature,
    maxTemperature,
    minHumidity,
    maxHumidity,
    targetMoisture,
    result
  }
}
