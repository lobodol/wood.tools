//import './assets/main.scss'
//import 'primevue/resources/themes/bootstrap4-light-blue/theme.css'
import '@/assets/styles.scss'
import '@/assets/tailwind.css'

import Aura from '@primevue/themes/aura'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config'
import Menubar from 'primevue/menubar'
import Message from 'primevue/message'
import Sidebar from 'primevue/sidebar'
import Button from 'primevue/button'
import SplitButton from 'primevue/splitbutton'
import Menu from 'primevue/menu'
import Dropdown from 'primevue/dropdown'
import Calendar from 'primevue/calendar'
import SelectButton from 'primevue/selectbutton'
import Card from 'primevue/card'
import Toast from 'primevue/toast'
import TabMenu from 'primevue/tabmenu'
import Dialog from 'primevue/dialog'
import Paginator from 'primevue/paginator'
import Password from 'primevue/password'
import InputText from 'primevue/inputtext'
import InputSwitch from 'primevue/inputswitch'
import ProgressSpinner from 'primevue/progressspinner'
import PanelMenu from 'primevue/panelmenu'
import DataTable from 'primevue/datatable'
import Toolbar from 'primevue/toolbar'
import Textarea from 'primevue/textarea'
import Column from 'primevue/column'
import InputNumber from 'primevue/inputnumber'
import AutoComplete from 'primevue/autocomplete'
import Skeleton from 'primevue/skeleton'
import Avatar from 'primevue/avatar'
import AvatarGroup from 'primevue/avatargroup'
import Breadcrumb from 'primevue/breadcrumb'
import Tooltip from 'primevue/tooltip'
import ToastService from 'primevue/toastservice'
import MultiSelect from 'primevue/multiselect'
import RadioButton from 'primevue/radiobutton'
import * as Plugins from '@/plugins'
const app = createApp(App)

app.use(router)
app.use(PrimeVue, {
  theme: {
    preset: Aura,
    options: {
      darkModeSelector: '.app-dark'
    }
  }
})

app.component('PAutoComplete', AutoComplete)
app.component('PAvatar', Avatar)
app.component('PAvatarGroup', AvatarGroup)
app.component('PButton', Button)
app.component('PSplitButton', SplitButton)
app.component('PCalendar', Calendar)
app.component('PCard', Card)
app.component('PColumn', Column)
app.component('PDialog', Dialog)
app.component('PDropdown', Dropdown)
app.component('PMultiSelect', MultiSelect)
app.component('PInputText', InputText)
app.component('PInputNumber', InputNumber)
app.component('PInputSwitch', InputSwitch)
app.component('PMenu', Menu)
app.component('PMenubar', Menubar)
app.component('PMessage', Message)
app.component('PDataTable', DataTable)
app.component('PPaginator', Paginator)
app.component('PPassword', Password)
app.component('PPanelMenu', PanelMenu)
app.component('PProgressSpinner', ProgressSpinner)
app.component('PSelectButton', SelectButton)
app.component('PSidebar', Sidebar)
app.component('PSkeleton', Skeleton)
app.component('PTabMenu', TabMenu)
app.component('PTextarea', Textarea)
app.component('PToast', Toast)
app.component('PToolbar', Toolbar)
app.component('PBreadcrumb', Breadcrumb)
app.component('PRadioButton', RadioButton)

app.directive('tooltip', Tooltip)

app.use(ToastService)
app.use(Plugins.i18n)

app.mount('#app')
