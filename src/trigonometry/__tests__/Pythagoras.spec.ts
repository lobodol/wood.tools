import { Pythagoras } from '../Pythagoras'

describe('Pythagoras theorem', () => {
  const pythagoras = new Pythagoras()

  test('Compute hypotenuse', () => {
    expect(pythagoras.hypotenuse(1, 1)).toEqual(Math.sqrt(2))
  })

  test('Compute adjacent side', () => {
    // Use toBeCloseTo due to float approximation
    expect(pythagoras.adjacent(Math.sqrt(2), 1)).toBeCloseTo(1)
  })
})
