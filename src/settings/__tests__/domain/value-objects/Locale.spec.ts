import { Locale } from '@/settings/domain/value-objects/Locale'

describe('Locale', (): void => {
  test('should default to first locale if none is supported', (): void => {
    const locale = new Locale('xx-yy')
    expect(locale.code).toBe('fr')
  })

  test('should not default to first locale if one is supported', (): void => {
    const locale = new Locale('en-gb')
    expect(locale.code).toBe('en-gb')
  })
})
