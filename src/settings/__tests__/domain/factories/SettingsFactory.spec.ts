import { SettingsFactory } from '@/settings/infrastructure/data/factories/SettingsFactory'

describe('SettingsFactory', (): void => {
  describe('Create from JSON', (): void => {
    test('Should create from invalid JSON', (): void => {
      const factory = new SettingsFactory('en-gb')
      const settings = factory.fromJSON({ _locale: 'xx-yy' })

      expect(settings.locale.code).toBe('fr')
    })

    test('Should create from valid JSON', (): void => {
      const factory = new SettingsFactory('en-gb')
      const settings = factory.fromJSON({ _locale: 'fr' })

      expect(settings.locale.code).toBe('fr')
    })
  })

  describe('Create from default', (): void => {
    test('Should create from default', (): void => {
      const factory = new SettingsFactory('en-gb')
      const settings = factory.create()

      expect(settings.locale.code).toBe('en-gb')
    })
  })
})
