import type { SettingsRepository } from '@/settings/domain/repositories/SettingsRepository'
import { Settings } from '@/settings/domain/entities/Settings'
import { SettingsFactory } from '@/settings/infrastructure/data/factories/SettingsFactory'

export class LocalStorageSettingsRepository implements SettingsRepository {
  private readonly KEY: string = 'settings'

  constructor(private readonly factory: SettingsFactory) {}

  save(settings: Settings) {
    localStorage.setItem(this.KEY, JSON.stringify(settings))
  }

  get(): Settings {
    let settings

    try {
      settings = this.factory.fromJSON(JSON.parse(localStorage.getItem(this.KEY) || '{}'))
    } catch (error) {
      settings = this.factory.create()
    }

    return settings
  }
}
