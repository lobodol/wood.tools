import { Settings } from '@/settings/domain/entities/Settings'
import { Locale } from '@/settings/domain/value-objects/Locale'

export class SettingsFactory {
  constructor(private readonly defaultLocale: string) {}

  public create(): Settings {
    return new Settings(new Locale(this.defaultLocale))
  }

  public fromJSON(json: { [key: string]: any }): Settings {
    return new Settings(new Locale(json._locale || ''))
  }
}
