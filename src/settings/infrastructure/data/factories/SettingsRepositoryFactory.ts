import type { SettingsRepository } from '@/settings/domain/repositories/SettingsRepository'
import { LocalStorageSettingsRepository } from '@/settings/infrastructure/data/LocalStorageSettingsRepository'
import { SettingsFactory } from '@/settings/infrastructure/data/factories/SettingsFactory'

export class SettingsRepositoryFactory {
  private static instance: SettingsRepository

  public static create(): SettingsRepository {
    if (!this.instance) {
      this.instance = new LocalStorageSettingsRepository(
        new SettingsFactory(window.navigator.language)
      )
    }

    return this.instance
  }
}
