import { ref, watch } from 'vue'
import type { LocaleOption } from '@/settings/presentation/dto/LocaleOption'
import { LocaleMapper } from '@/settings/presentation/mappers/LocaleMapper'
import { updateLocale } from '@/plugins/i18n'
import { useI18n } from 'vue-i18n'
import { SettingsRepositoryFactory } from '@/settings/infrastructure/data/factories/SettingsRepositoryFactory'
import { SUPPORTED_LANG } from '@/settings/domain/value-objects/Locale'

export const useSettingsView = () => {
  const { t } = useI18n()
  const repository = SettingsRepositoryFactory.create()
  const mapper = new LocaleMapper(t)

  const settings = repository.get()

  // View variables
  const locales = ref<LocaleOption[]>(SUPPORTED_LANG.map((code) => mapper.domainToView(code)))
  const selectedLocale = ref<LocaleOption>(mapper.domainToView(settings.locale.code)) // Defaults to the saved locale

  watch(selectedLocale, () => {
    settings.locale = mapper.viewToDomain(selectedLocale.value)
    repository.save(settings)

    updateLocale(selectedLocale.value.code)

    // Re-compute options
    locales.value = SUPPORTED_LANG.map((code) => mapper.domainToView(code))

    // Update label of selected locale
    selectedLocale.value.name = mapper.domainToView(settings.locale.code).name
  })

  return {
    locales,
    selectedLocale
  }
}
