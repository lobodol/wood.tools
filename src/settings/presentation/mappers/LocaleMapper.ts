import type { LocaleOption } from '@/settings/presentation/dto/LocaleOption'
import type { Lang } from '@/settings/domain/value-objects/Locale'
import { Locale } from '@/settings/domain/value-objects/Locale'

export class LocaleMapper {
  constructor(private readonly t: (x: string) => string) {}

  viewToDomain(locale: LocaleOption): Locale {
    return new Locale(locale.code)
  }

  domainToView(locale: Lang): LocaleOption {
    return {
      name: this.t(`lang.${locale}`),
      code: locale
    }
  }
}
