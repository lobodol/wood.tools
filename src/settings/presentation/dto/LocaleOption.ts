import type { Lang } from '@/settings/domain/value-objects/Locale'

export type LocaleOption = {
  name: string
  code: Lang
}
