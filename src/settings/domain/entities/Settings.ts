import type { Lang } from '@/settings/domain/value-objects/Locale'
import { Locale } from '@/settings/domain/value-objects/Locale'

export class Settings {
  private _locale: Lang

  constructor(locale: Locale) {
    this._locale = locale.code
  }

  set locale(locale: Locale) {
    this._locale = locale.code
  }

  get locale(): Locale {
    return new Locale(this._locale)
  }
}
