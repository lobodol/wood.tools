export const SUPPORTED_LANG = ['fr', 'en-gb'] as const

export type Lang = (typeof SUPPORTED_LANG)[number]

export class Locale {
  readonly code: Lang

  constructor(code: string) {
    this.code = this.languageNegotiation(code)
  }

  private languageNegotiation(accept: string): Lang {
    const index = SUPPORTED_LANG.indexOf(accept as Lang)
    const defaultLocale: Lang = SUPPORTED_LANG[0]

    return index >= 0 ? SUPPORTED_LANG[index] : defaultLocale
  }
}
