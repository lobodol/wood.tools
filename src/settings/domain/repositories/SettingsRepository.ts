import type { Settings } from '@/settings/domain/entities/Settings'

export interface SettingsRepository {
  save(settings: Settings): void

  get(): Settings
}
