# Contibuting
Wood.tools is an open source project.

If you would like to contribute, please read the following guidelines.

## Reporting a bug
Whenever you find a bug in Wood.tools, we kindly ask you to report it. It helps us make it better.

To do so, simply [create a new issue](https://gitlab.com/dolmen-public/wood.tools/-/issues/new), following some basic rules:

- Use the title field to clearly describe the issue
- Describe the steps needed to reproduce the bug with short code examples (providing a unit test that illustrates the bug is best)
- If the bug you experienced is not simple or affects more than one layer, providing a simple failing unit test may not be sufficient. In this case, please provide a reproducer
- Give as much detail as possible about your environment (OS, Docker version, enabled extensions, ...)

## Proposing a change
A merge request, "MR" for short, is the best way to provide a bug fix or to propose enhancements to Wood.tools.

Create a new branch from `main`, implement your feature/fix and create a new MR.

## Standard commit message
Commits **must** follow the [Conventional commits rules](https://www.conventionalcommits.org).

| Type     | Description                                                                                                            |
| :------- | :--------------------------------------------------------------------------------------------------------------------- |
| refactor | Changes which neither fix a bug nor add a feature                                                                      |
| fix      | Changes which patch a bug                                                                                              |
| feat     | Changes which introduce a new feature                                                                                  |
| build    | Changes which affect the build system or external dependencies.<br/>Example scopes: gulp, broccoli, npm                |
| chore    | Changes which aren't user-facing                                                                                       |
| style    | Changes which don't affect code logic, such as white-spaces, formatting, missing semi-colons                           |
| test     | Changes which add missing tests or correct existing tests                                                              |
| docs     | Changes which affect documentation                                                                                     |
| perf     | Changes which improve performance                                                                                      |
| ci       | Changes which affect CI configuration files and scripts.<br/>Example scopes: travis, circle, browser-stack, sauce-labs |
| revert   | Changes which revert a previous commit                                                                                 |

## Coding standards
Wood.tools code may be contributed by anyone around the world. To make every piece of code look and feel familiar, some coding standards are defined that all contributions must follow.

Thes coding standards are enforeced using [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/).

### Make your code follow the coding standards
Instead of reviewing your code manually, just run the following make command:


```bash
make lint
```

Under the hood, it runs the following Docker command:

```bash
docker-compose run --rm -l 'traefik.enable=false' app yarn lint
```
