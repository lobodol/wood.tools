/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  reporters: [
    'jest-junit',
    ['jest-html-reporters', { publicPath: './coverage', filename: 'tests.html' }]
  ],
  coverageReporters: ['lcov', 'cobertura', 'text-summary']
}
